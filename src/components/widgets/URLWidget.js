import React from "react";

export default ({value}) => <a href={value} target="_blank" rel="noopener noreferrer">{value}</a>;
