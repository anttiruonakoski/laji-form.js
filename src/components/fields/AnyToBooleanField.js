import { anyToBoolean } from "../widgets/AnyToBooleanWidget";
export default anyToBoolean(!"field");
