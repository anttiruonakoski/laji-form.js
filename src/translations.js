﻿export default {
	submit: {
		fi: "Lähetä",
		en: "Submit",
		sv: "Sänd"
	},
	cancel: {
		fi: "Peru",
		en: "Cancel",
		sv: "Avbryt"
	},
	today: {
		fi: "Tänään",
		en: "Today",
		sv: "Idag"
	},
	yesterday: {
		fi: "Eilen",
		en: "Yesterday",
		sv: "Igår"
	},
	datePlaceholderDay: {
		fi: "pp.kk.vvvv",
		en: "dd/mm/yyyy",
		sv: "pp.kk.vvvv"
	},
	timePlaceholderDay: {
		fi: "tt.mm",
		en: "h:mm AM/PM",
		sv: "tt.mm"
	},
	yes: {
		fi: "Kyllä",
		en: "Yes",
		sv: "Ja"
	},
	no: {
		fi: "Ei",
		en: "No",
		sv: "Nej"
	},
	knownSpeciesName: {
		fi: "Tunnettu nimi",
		en: "Known name",
		sv: "Känt namn"
	},
	unknownSpeciesName: {
		fi: "Tuntematon nimi",
		en: "Unknown name",
		sv: "Okänt namn"
	},
	unknownName: {
		fi: "Ei tunnettu nimi",
		en: "Not known name",
		sv: "Okänt namn"
	},
	fix: {
		fi: "Korjaa",
		en: "Fix",
		sv: "Fixa"
	},
	or: {
		fi: "Tai",
		en: "Or",
		sv: "Eller"
	},
	and: {
		fi: "Ja",
		en: "And",
		sv: "Och"
	},
	useUnknownName: {
		fi: "Käytä tuntematonta nimeä",
		en: "Use unknown name",
		sv: "Använd okänt namn"
	},
	search: {
		fi: "Etsi",
		en: "Search",
		sv: "Sök"
	},
	filter: {
		fi: "Rajaa",
		en: "Filter",
		sv: "Filtrera"
	},
	more: {
		fi: "Lisää",
		en: "More",
		sv: "Mera"
	},
	selectOneOfTheFollowing: {
		fi: "Valitse yksi seuraavista",
		en: "Select one of the following",
		sv: "Välj en av följande"
	},
	openSpeciedCard: {
		fi: "Avaa lajikortti",
		en: "Open taxon information",
		sv: "Öppna artkort"
	},
	remove: {
		fi: "Poista",
		en: "Remove",
		sv: "Radera"
	},
	close: {
		fi: "Sulje",
		en: "Close",
		sv: "Stäng"
	},
	confirmRemove: {
		fi: "Haluatko varmasti poistaa?",
		en: "Are you sure you want to remove?",
		sv: "Är du säker på att du vill radera?"
	},
	map: {
		fi: "Kartta",
		en: "Map",
		sv: "Karta"
	},
	place: {
		fi: "Paikka",
		en: "Place",
		sv: "Plats"
	},
	dropOrSelectFiles: {
		fi: "Raahaa tähän kuvat tai klikkaa valitaksesi kuvat koneeltasi",
		en: "Drag pictures here or click here to select the pictures from you computer",
		sv: "Dra bilder här eller klicka här för att välja bilder från din dator"
	},
	selectMoreFields: {
		fi: "Valitse lisää kenttiä",
		en: "Select more fields",
		sv: "Välj flera fält"
	},
	detachUnit: {
		fi: "Irrota havainto uudelle karttakuviolle.",
		en: "Detach the observation to a new map feature",
		sv: "Lossa observationen till en ny kartafigur"
	},
	detachUnitHelp: {
		fi: "Piirrä uusi kuvio irrotettavalle havainnolle.",
		en: "Draw a new feature for the observation being detached.",
		sv: "Rita en ny figur för den observationen som lossnat."
	},
	chooseDate: {
		fi: "Valitse päivämäärä",
		en: "Choose a date",
		sv: "Välj datum"
	},
	chooseTime: {
		fi: "Valitse kellonaika",
		en: "Choose the time",
		sv: "Välj tiden"
	},
	unitsPartitive: {
		fi: "Havaintoa",
		en: "Observations",
		sv: "Observationer"
	},
	setLocation: {
		fi: "Aseta sijainti",
		en: "Set a location",
		sv: "Ange platsen"
	},
	currentLocation: {
		fi: "Nykyinen sijainti",
		en: "Current location",
		sv: "Aktuella platsen"
	},
	noResults: {
		fi: "Ei tuloksia",
		en: "No results",
		sv: "Inga resultat"
	},
	add: {
		fi: "Lisää",
		en: "Add",
		sv: "Lägg"
	},
	all: {
		fi: "Kaikki",
		en: "All",
		sv: "Alla"
	},
	saveFail: {
		fi: "Tallennus epäonnistui.",
		en: "Saving failed.",
		sv: "Sparade framgångsrikt!"
	},
	tryAgainLater: {
		fi: "Yritä myöhemmin uudestaan.",
		en: "Please try again later.",
		sv: "Försök igen senare."
	},
	saveSuccess: {
		fi: "Tallennus onnistui!",
		en: "Saved successfully!",
		sv: "Lagring lyckades!"
	},
	Mb: {
		fi: "Mt",
		en: "Mb",
		sv: "Mb"
	},
	allowedFileFormats: {
		fi: "Sallitut kuvaformaatit ovat",
		en: "Allowed image formats are",
		sv: "Tillåtna filformat för bilderna är"
	},
	allowedFileSize: {
		fi: "Kuvan suurin sallittu koko on",
		en: "The maximum allowed file size is",
		sv: "Den största tillåtna storleken på en bild är"
	},
	invalidFile: {
		fi: "Kuvatiedosto oli virheellinen.",
		en: "The file was invalid.",
		sv: "Filen var ogiltig."
	},
	insufficientSpace: {
		fi: "Kuvapalvelin ei pysty käsittelemään latauspyyntöä riittämättömän levytilan vuoksi.",
		en: "The image server can't process the upload request because of insufficient space.",
		sv: "Bildservern kan inte bearbeta uppladdnings begäran på grund av otillräckligt utrymme."
	},
	filesLengthDiffer: {
		fi: "Kaikki kuvat eivät tallentuneet. Tarkista kuvat.",
		en: "All the images weren't saved. Please verify the images.",
		sv: "Alla bilder sparades inte. Vänligen kontrollera bilderna."
	},
	error: {
		fi: "Virhe",
		en: "Error",
		sv: "Fel"
	},
	next: {
		fi: "Seuraava",
		en: "Next",
		sv: "Nästa"
	},
	previous: {
		fi: "Edellinen",
		en: "Previous",
		sv: "Föregående"
	},
	invalidUnitCode: {
		fi: "Virheellinen koodi.",
		en: "Invalid code.",
		sv: "Ogiltig kod."
	},
	open: {
		fi: "Avaa",
		en: "Open",
		sv: "Öppna"
	},
	shortcuts: {
		fi: "Pikanäppäimet",
		en: "Shortcuts",
		sv: "Kortkommandon"
	},
	navigate: {
		fi: "Liiku seuraavaan kenttään",
		en: "Navigate to the next field",
		sv: "Navigera till nästa fält"
	},
	navigateReverse: {
		fi: "Liiku edelliseen kenttään",
		en: "Navigate to the previous field",
		sv: "Navigera till föregående fält"
	},
	navigateArray: {
		fi: "Liiku seuraavaan toisteiseen kenttään",
		en: "Navigate to the next array item",
		sv: "Navigera till nästa upprepande fält"
	},
	navigateArrayReverse: {
		fi: "Liiku edelliseen toisteiseen kenttään",
		en: "Navigate to the previous array item",
		sv: "Navigera till föregående upprepande fält"
	},
	insert: {
		fi: "Lisää uusi kenttä",
		en: "Add a new item to an array",
		sv: "Lägg till ett nytt fält"
	},
	delete: {
		fi: "Poista toisteinen kenttä",
		en: "Remove the focused array item",
		sv: "Radera det upprepande fältet"
	},
	insertTarget: {
		fi: "Lisää uusi",
		en: "Add a new",
		sv: "Lägg till ny"
	},
	navigateArrayTarget: {
		fi: "Liiku seuraavaan",
		en: "Navigate to the next",
		sv: "Navigera till nästa"
	},
	navigateArrayReverseTarget: {
		fi: "Liiku edelliseen",
		en: "Navigate to the previous",
		sv: "Navigera till föregående"
	},
	textareaHint: {
		fi: "Lisää uuden rivin tekstikenttään",
		en: "Adds a new row to the textarea",
		sv: "Lägger till en ny rad i textområdet"
	},
	splitLineTransectByMetersTarget: {
		fi: "Pätkäise linja metriluvulla",
		en: "Split line transect by meters",
		sv: "Bryta av linjen gemon meter"
	},
	setLocationToUnit: {
		fi: "Aseta havainnolle uusi sijainti",
		en: "Set a new location for the observation",
		sv: "Ange en ny plats för observationen"
	},
	below: {
		fi: "alla",
		en: "below",
		sv: "under"
	},
	errors: {
		fi: "virheet",
		en: "errors",
		sv: "fel"
	},
	warnings: {
		fi: "varoitukset",
		en: "warnings",
		sv: "varningar"
	},
	revalidate: {
		fi: "Päivitä virheet",
		en: "Refresh the errors",
		sv: "Uppdatera felen"
	},
	submitWithWarnings: {
		fi: "Tallenna varoituksista huolimatta"
	},
	chooseFromNamedPlace: {
		fi: "Käytä omaa paikkaa",
		en: "Use an own place",
		sv: "Använd en egen plats"
	},
	chooseNamedPlace: {
		fi: "Valitse oma paikka",
		en: "Choose an own place",
		sv: "Välj en egen plats"
	},
	namedPlacesFetchFail: {
		fi: "Omien paikkojen hakeminen epäonnistui.",
		en: "Fetching own places failed",
		sv: "Hämtning av egna platser misslyckades"
	},
	namedPlacesSaveFail: {
		fi: "Oman paikan tallennus epäonnistui.",
		en: "Failed to save the place",
		sv: "Misslyckades med att spara platsen"
	},
	list: {
		fi: "Luettelo",
		en: "List",
		sv: "Lista"
	},
	name: {
		fi: "Nimi",
		en: "Name",
		sv: "Namn"
	},
	notes: {
		fi: "Lisätiedot",
		en: "Details",
		sv: "Tilläggsuppgifter"
	},
	useThisPlace: {
		fi: "Käytä tätä paikkaa",
		en: "Use this place",
		sv: "Använd denna plats"
	},
	SelectPlaceFromList: {
		fi: "Kirjoita valitaksesi paikan listalta",
		en: "Type in order to select a place from the list",
		sv: "Skriv in för att välja en plats från listan"
	},
	warning: {
		fi: "Varoitus",
		en: "Warning",
		sv: "Varning"
	},
	usedNamedPlaceName: {
		fi: "Paikan nimi on jo käytössä",
		en: "The place name is used already",
		sv: "Platsen heter redan"
	},
	save: {
		fi: "Tallenna",
		en: "Save",
		sv: "Spara"
	},
	new: {
		fi: "Uusi",
		en: "New",
		sv: "Ny"
	},
	saveCurrentOverwrite: {
		fi: "Tallenna nykyisen päälle",
		en: "Overwrite existing",
		sv: "Skriv över befintliga"
	},
	saveNamedPlace: {
		fi: "Tallenna omana paikkana",
		en: "Save as an own place",
		sv: "Spara som en egen plats"
	},
	clickPlaceToOverwrite: {
		fi: "Klikkaa paikkaa ylikirjoittaaksesi se",
		en: "Click a place in order to overwrite it",
		sv: "Klicka på en plats för att skriva över den"
	},
	namedPlacesUseFail: {
		fi: "Paikka ei ole yhteensopiva lomakkeen kanssa.",
		en: "The place is not compatible with the form",
		sv: "Platsen är inte kompatibel med formuläret"
	},
	typeError: {
		fi: "Pitää olla ",
		en: "Must be of type ",
		sv: "Måste vara "
	},
	integer: {
		fi: "kokonaisluku",
		en: "integer",
		sv: "ett heltal"
	},
	string: {
		fi: "merkkijono",
		en: "string",
		sv: "stringen"
	},
	fieldIsRequired: {
		fi: "Pakollinen kenttä",
		en: "Required field",
		sv: "Obligatoriskt fält"
	},
	namedPlaces: {
		fi: "Omat paikat",
		en: "Own places",
		sv: "Egna platser"
	},
	usedNamedPlaceNameMultiple: {
		fi: "Valitse alta ylikirjoitettava paikka",
		en: "Select a place to overwrite below",
		sv: "Välj en plats att skriva över nedan"
	},
	saveExistingOverwrite: {
		fi: "Ylikirjoita paikka",
		en: "Overwrite place",
		sv: "Skriv över platsen"
	},
	pickInformalTaxonGroup: {
		fi: "Valitse lajiryhmä",
		en: "Select a species group",
		sv: "Välj ornagismgruppen"
	},
	select: {
		fi: "Valitse",
		en: "Select",
		sv: "Välj"
	},
	showSubgroups: {
		fi: "Näytä alaryhmät",
		en: "Show subgroups",
		sv: "Välj subgrupper"
	},
	geolocate: {
		fi: "Hae paikkatiedot",
		en: "Get place data",
		sv: "få platsdata"
	},
	mapFullscreen: {
		fi: "Avaa kartta kokoruututilassa",
		en: "Open the map in fullscreen",
		sv: "Öppna kartan i fullskärm"
	},
	mapExitFullscreen: {
		fi: "Poistu kokoruututilasta",
		en: "Exit fullscreen mode",
		sv: "Avsluta helskärmsläge"
	},
	openShorthand: {
		fi: "Avaa pikasyöttötila",
		en: "Open shorthand mode",
		sv: "Öppet stenografi läge"
	},
	closeShorthand: {
		fi: "Sulje pikasyöttötila",
		en: "Close shorthand mode",
		sv: "Nära stenografi läge"
	},
	startShorthand: {
		fi: "Aloita pikasyöttö",
		en: "Start shorthand mode",
		sv: "Start stenografi läge"
	},
	stopShorthand: {
		fi: "Lopeta pikasyöttö",
		en: "Stop shorthand mode",
		sv: "Slut stenografi läge"
	},
	shorthandHelp: {
		fi: "Kirjoitusmuoto: (määrä) LAJI (koiraiden yksilömäärä) (naaraiden yksilömäärä). Suluilla merkityt eivät ole pakollisia.",
		en: "Format: (count) SPECIES (male individual count) (female individual count). The fields marked with brackets aren't required",
		sv: "Formatera: (antal) SPECIER (individantal av hane) (individantal av hone). Fälten markerade med parentes är inte nödvändiga."
	},
	finland: {
		fi: "Suomi",
		en: "Finland",
		sv: "Finska"
	},
	unitAutosuggestFieldTogglePlaceholder: {
		fi: "(määrä) LAJI (koir. yksilöm.) (naar. yksilöm.)",
		en: "(count) SPECIES (male count) (female count)",
		sv: "(antal) SPECIER (ind. av hane) (ind. av hone)"
	},
	lineTransectSegmentHasErrors: {
		fi: "Pätkällä on virheitä",
		en: "Segment has errors",
		sv: "Segmentet har fel"
	},
	lineTransectSegmentNotCounted: {
		fi: "Pätkää ei lasketa",
		en: "Segment isn't counted",
		sv: "Segmentet räknas inte"
	},
	comments: {
		fi: "Kommentit",
		en: "Comments",
		sv: "Kommenter"
	},
	showAnnotations: {
		fi: "Näytä annotaatiot",
		en: "Show annotations",
		sv: "Välj annotationer"
	},
	deleteFail: {
		fi: "Poisto epäonnistui.",
		en: "Delete failed.",
		sv: "Radera framgångsrikt!"
	},
	showLineTransectControls: {
		fi: "Näytä lisää työkaluja",
		en: "Show more tools",
		sv: "Näytä lisää työkaluja"
	},
	hideLineTransectControls: {
		fi: "Piilota työkalut",
		en: "Hide tools",
		sv: "Piilota työkalut"
	}
};
